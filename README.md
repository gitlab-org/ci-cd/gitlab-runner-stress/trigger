# Trigger

Run every pipeline available inside of
`https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress`.

This project is used by
[`gitlab-runner-stress`](https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-stress/gitlab-runner-stress)
binary which creates different kinds of pipelines.
